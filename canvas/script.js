(function() {
  
  'use strict';
  
  var isDrawing, lastPoint;
  var container    = document.getElementById('js-container'),
      canvas       = document.getElementById('js-canvas'),
      canvasWidth  = canvas.width,
      canvasHeight = canvas.height,
      ctx          = canvas.getContext('2d'),
      image        = new Image(),
      brush        = new Image();
      
  
  brush.src = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAA
  DsMAAA7DAcdvqGQAAACcSURBVChTY0ADjFAaDkACIPwfxPn//z83kDIG4rdAfIuRkfE3SBysCygZuHDhwlOrVq16AGRPBmJlkDgY/Pnzx2/evHmH16xZ8wQocQGI04GYHYjB
  Ov3nzp17CKoThCcBsQhULwNTe3u7LwcHh0BoaOgvIP8QEE8AYpAbIABofBhQx3EgngXE6kDMBJWCAKAAHxC7AbEEhiQDAwMATOtjNPuYYuUAAAAASUVORK5CYII=`;
  
  canvas.addEventListener('touchstart', handleMouseDown, false);
  canvas.addEventListener('touchmove', handleMouseMove, false);
  canvas.addEventListener('touchend', handleMouseUp, false);

  canvas.addEventListener('mousemove', handleMouseMove, false);
  canvas.addEventListener('mousedown', handleMouseDown, false);
  canvas.addEventListener('mouseup', handleMouseUp, false);
  canvas.addEventListener('mouseleave', handleMouseUp, false);
  
  function distanceBetween(point1, point2) {
    return Math.sqrt(Math.pow(point2.x - point1.x, 2) + Math.pow(point2.y - point1.y, 2));
  }
  
  function angleBetween(point1, point2) {
    return Math.atan2( point2.x - point1.x, point2.y - point1.y );
  }
  
  
  function getMouse(e, canvas) {
    var offsetX = 0, offsetY = 0, mx, my;

    if (canvas.offsetParent !== undefined) {
      do {
        offsetX += canvas.offsetLeft;
        offsetY += canvas.offsetTop;
      } while ((canvas = canvas.offsetParent));
    }

    mx = (e.pageX || e.touches[0].clientX) - offsetX;
    my = (e.pageY || e.touches[0].clientY) - offsetY;

    return {x: mx, y: my};
  }
  
  
  function handleMouseDown(e) {
    isDrawing = true;
    lastPoint = getMouse(e, canvas);
  }

  function handleMouseMove(e) {
    if (!isDrawing) { return; }
    
    e.preventDefault();

    var currentPoint = getMouse(e, canvas),
      dist = distanceBetween(lastPoint, currentPoint),
      angle = angleBetween(lastPoint, currentPoint),
      x, y;
    
    for (var i = 0; i < dist; i++) {
      x = lastPoint.x + (Math.sin(angle) * i) - 25;
      y = lastPoint.y + (Math.cos(angle) * i) - 25;
      ctx.globalCompositeOperation = 'source-over';
      ctx.drawImage(brush, x, y);
    }
    
    lastPoint = currentPoint;
  }

  function handleMouseUp(e) {
    isDrawing = false;
  }
  
})();